﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsSelector : MonoBehaviour
{
    public GameObject[] _models;
    private Animator[] _anims;
    public GameObject[] _botones;
    [Range(0,5)]
    public float _coolDown;
    private float _time;
    private int _seleccion;

    private void Start()
    {
        _seleccion = 1;
        _time = _coolDown;

        _anims = new Animator[_models.Length];
        for (int i = 0; i < _models.Length; i++)
        {
            _anims[i] = _models[i].GetComponent<Animator>();
        }
    }
    private void Update()
    {
        for (int i = 0; i < _models.Length; i++)
        {
            _anims[i].SetFloat("_timeOpen", _time);

            if (i == _seleccion - 1)
            {
                if (_time <= _coolDown/2)
                    _models[i].SetActive(true);
                else
                    _time -= Time.deltaTime; 
            }
            else
            {
                if (_time <= _coolDown/2)
                    _models[i].SetActive(false);
            }
        }
    }

    private void OnBecameVisible()
    {
        for (int i = 0; i < _botones.Length; i++)
        {
            _botones[i].SetActive(true);
        }
    }
    private void OnBecameInvisible()
    {
        for (int i = 0; i < _botones.Length; i++)
        {
            _botones[i].SetActive(false);
        }
    }

    public void Boton(int _num)
    {
        _seleccion = _num;
        _time = _coolDown;
    }
}
