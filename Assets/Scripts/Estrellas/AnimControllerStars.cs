﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControllerStars : MonoBehaviour
{
    public Animator _anim;
    private bool _startAnim;
    public GameObject[] _bright;
    public GameObject[] _stars;

    private void Update()
    {
        _anim.SetBool("_see", _startAnim);
        for (int i = 0; i < _bright.Length; i++)
        {
            _bright[i].GetComponent<LensFlare>().brightness = (_stars[i].transform.localScale.x / 100) + 0.1f;
        }
    }
    private void OnBecameVisible()
    {
        _startAnim = true;
        for (int i = 0; i < _bright.Length; i++)
        {
            _bright[i].SetActive(true);
        }
    }
    private void OnBecameInvisible()
    {
        _startAnim = false;
        for (int i = 0; i < _bright.Length; i++)
        {
            _bright[i].SetActive(false);
        }
    }
}
