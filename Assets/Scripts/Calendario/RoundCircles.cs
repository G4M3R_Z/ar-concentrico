﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundCircles : MonoBehaviour
{
    public float _speed;
    [Range(1,2)]
    public float _growSpeed = 1;
    public Calendario _calendario;
    [Range(0,100)]
    public float _limit;
    public SkinnedMeshRenderer _circulo;
    public bool _grow;
    private float _number;


    void Start()
    {
        _number = _limit;
    }
    private void Update()
    {
        if(_calendario._see)
        {
            //Rotar
            transform.Rotate(0, 0, _speed * Time.deltaTime);

            //Morpher
            _circulo.SetBlendShapeWeight(0, _number);

            if (_grow)
            {
                if (_number > 0)
                {
                    _number -= Time.deltaTime * 50 * _growSpeed;
                }
                else
                {
                    _number = 0;
                }
            }
            else
            {
                if (_number < _limit)
                {
                    _number += Time.deltaTime * 50 * _growSpeed;
                }
                else
                {
                    _number = _limit;
                }
            }
        }
    }
}
