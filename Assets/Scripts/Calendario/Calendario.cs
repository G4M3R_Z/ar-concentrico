﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calendario : MonoBehaviour
{
    public GameObject[] _canvasButtons;
    public bool _see;

    public RoundCircles[] _circulos;
    public int _botonNum;

    private void Update()
    {
        if (_see)
        {
            for (int i = 0; i < _circulos.Length; i++)
            {
                if (i == _botonNum - 1)
                {
                    _circulos[i]._grow = true;
                }
                else if(_botonNum != 4)
                {
                    _circulos[i]._grow = false;
                }
                else
                {
                    _circulos[i]._grow = true;
                }
            }
        }
    }

    #region Aparecer
    private void OnBecameVisible()
    {
        _see = true;

        for (int i = 0; i < _canvasButtons.Length; i++)
        {
            _canvasButtons[i].SetActive(true);
        }
    }
    private void OnBecameInvisible()
    {
        _see = false;

        for (int i = 0; i < _canvasButtons.Length; i++)
        {
            _canvasButtons[i].SetActive(false);
        }
    }
    #endregion

    public void Boton(int _num)
    {
        _botonNum = (_botonNum != _num) ? _botonNum = _num : _botonNum = 0;
    }
}
