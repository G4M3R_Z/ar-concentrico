﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CanvasSwitcher : MonoBehaviour
{
    public GameObject[] _canvas;

    [Range(1,2)]
    public int _selector;

    private void Start()
    {
        this.enabled = false;
    }
    private void Update()
    {
        for (int i = 0; i < _canvas.Length; i++)
        {
            if (i == _selector - 1)
                _canvas[i].SetActive(true);
            else
                _canvas[i].SetActive(false);
        }
    }
}
